### Library of infrastructure deployment routines

There are components in several categories:

  * CI/CD deployment setup helpers: build.sh & setup.sh
    These shell scripts are designed to work in conjunction with pipeline configurations

  * folder 'tf': modular terraform code to be called from deployment roots

    - tf/modules: generic terraform code free of implicit assumptions on the calling context.
      All data needed by a module is passed in thru terraform input parameters.

    - tf/profiles: terraform code that may expect its calling context to be structured in a particular way, for instance site naming conventions, site-specific design constraints etc.

    - tf/roots: terraform code that is ready-to-run as an deployment routine to roll out a fully formed infrastructure component that only needs some amount of input parameters.

  * folder 'pipeline': azure devops pipeline templates. Currently, templates are available for standard terraform deployments that make use of the azure devops service connection and variable groups objects that are provisioned by the landing-zone deployment routines.


### Azure/devops pipeline environment

The terraform-library pipeline templates arrange a specific terraform runtime environment. This has three parts:

  * a terraform backend configuration file that points at a ready-to-use platform service storage location to store the terraform state file. This location is prepared for all landingzones during the landingzone deployment (see repository *azure-lz-deploy* for details).

  * a collection of environment variables to assist terraform runs. These include various terraform input variables that are part the terraform boilerplate code (see <elsewhere>).

  * a specific filesystem layout on the pipeline agents that affords terraform code the option of referring to modules in libraries loaded from auxiliary repositories by stable relative paths. The layout with respect to the pipeline-provided base directory (itself available in pipeline variable PIPELINE_WORKSPACE) using the example name *indigo* as the name of the workload repository, is as follows:

```
  ./lib       ## the content of repo terraform-library is mapped here
  ./conf      ## the content of repo azure-platform-configuration is mapped here
  ./indigo    ## the content of the workload repo 'indigo'
```

Suppose further that the terraform root module within *indigo* is in the subfolder **tfmain**, than it follows that a terraform module in the *terraform-library* may be refered to with a source path that starts with **../../lib/**.


##### Example *azure-pipelines.yaml' fragment illustrating the use of these terraform templates

```yaml
## declare auxiliary repositories
resources:
  repositories:
    - repository: lib
      type: git
      name: landingzone-infra/terraform-library
      ref: refs/heads/release-v1
    - repository: conf
      type: git
      name: landingzone-infra/azure-platform-configuration
      ref: refs/heads/main

## fetch variable group and service connection prepared by LZ provisioning
## Note: in this section '<name>' is a place-holder for the LZ name
variables:
  - group: 'vargroup-<name>-system'
  - name: mainWorkingDirectory
    value: $(System.DefaultWorkingDirectory)/$(Build.Repository.Name)
  - name: serviceConnection
    value: azurerm-<name>

stages:
- template: pipeline/std-tf-stages.yaml@lib
  parameters:
    workingDirectory: $(mainWorkingDirectory)
    serviceConnection: $(serviceConnection)
    # if needed, explicitly pass secrets (which are not automatically
    # included as environment variables)
    #envmap:
    #  TF_VAR_your_stuff: $(TF_VAR_your_stuff)

```


### Component versioning

Note that terraform module and profile names are versioned (with a '-vX' suffix). This is done to reduce the risk of terraform getting confused by too many internal layout changes. So if any major changes are made that may keep terraform from reconciling the existing state of resources that are deployed already then introduce a new version of module/profile so existing terraform deployment are not affected.
