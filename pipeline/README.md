###  Azure devops pipeline templates

In general deployments require data and code from auxiliary repositories that will need to be checkout along with the repository that actually triggers a pipeline. The templates provided in this library assume a specific agent filesystem layout to refer to repository content:

  * **$(Pipeline.Workspace)/$(Build.RepositoryName)**  - repo triggering this build
  * **$(Pipeline.Workspace)/conf**                     - azure-platform-configuration
  * **$(Pipeline.Workspace)/lib**                      - terraform-library


To achieve this, there must be a *resources:* declaration in the main pipeline confiuguration file (for instance *azure-pipelines.yaml*), like this:

```
resources:
  repositories:
    - repository: lib
      type: git
      name: landingzone-infra/terraform-library
      ref: refs/heads/feature/artifact-setup
    - repository: conf
      type: git
      name: landingzone-infra/azure-platform-configuration
      ref: refs/heads/main
```

Then, each pipeline job that refers to these auxiliary repositories shoud include a step to check all repositories to the expected filesystem location. E.g. with pipeline steps like these:

```
  - job: init
    steps:
    - checkout: self
    - checkout: lib
      path: s/lib
    - checkout: conf
      path: s/conf
```

When using the terraform step templates provided here, these checkout steps are already done in the templates when needed.
