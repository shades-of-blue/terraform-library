#
## Store secret in key vault and arrange access to list of reader principals.
## Parameters:
##  kvid                 - key vault identifier to store secret in
##  acl_list             - principals that get key vault access
##                         The entries in this list are tagged:
##                          'upn:' for user principal names
##                          'grp:' for group display names
##                          'svc:' for service principal display names
##  kv_secret_role_name  - the key vault role to assign to acl principals
##  values_bundle        - list of objects, each describing a key vault entry
##
## Principals in the acl list may retrieve secrets by name, for instance
## with powershell:
##  $secret = Get-AzKeyVaultSecret -VaultName "kvaadstage" -Name "aad-user-sa-hobgoblin-eur-edu-secret" -AsPlainText
##
## or with AZ CLI:
##
##   az keyvault secret show --vault-name "kvaadstage" --name "aad-user-sa-hobgoblin-eur-edu-secret" --query "value"
##
#
variable "kvid" {}
variable "acl_list" {
  default = []
  nullable = true
}
variable "kv_secret_role_name" {
  default = "Key Vault Secrets User"
}

variable "values_bundle" {
  type = list(map(any))
}


locals {
  ## split acl name list by tag
  acl_unames = flatten([
    for p in try(var.acl_list,[]):
      regexall("upn:(.*)", p)
  ])
  acl_gnames = flatten([
    for p in try(var.acl_list,[]):
      regexall("grp:(.*)", p)
  ])
  acl_snames = flatten([
    for p in try(var.acl_list,[]):
      regexall("svc:(.*)", p)
  ])

  ## lookup azure/ad objects for each tag
  acl_uobjs = {
    for u in local.acl_unames:
      u => data.azuread_user.adobject[u]
  }
  acl_gobjs = {
    for g in local.acl_gnames:
      g => data.azuread_group.adobject[g]
  }
  acl_sobjs = {
    for s in local.acl_snames:
      s => data.azuread_service_principal.adobject[s]
  }
}

module "kventries" {
  for_each      = { for v in var.values_bundle: (v.label) => v.value }
  source        = "./kvhelper"
  key_vault_id  = var.kvid
  acls          = merge(local.acl_uobjs,local.acl_gobjs,local.acl_sobjs)
  role          = var.kv_secret_role_name
  label         = each.key
  value         = each.value
}

## Lookup user and group objects
data "azuread_user" "adobject" {
  for_each            = { for u in local.acl_unames: u => {} }
  user_principal_name = each.key
}

data "azuread_group" "adobject" {
  for_each         = { for g in local.acl_gnames: g => {} }
  display_name     = each.key
  security_enabled = true
}

data "azuread_service_principal" "adobject" {
  for_each         = { for s in local.acl_snames: s => {} }
  display_name     = each.key
}
