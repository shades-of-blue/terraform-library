#
## Module: declare a key vault value and zero or more access assignments on it
#
variable "key_vault_id" {}
variable "acls"         {}
variable "role"         {}
variable "label"        {}
variable "value"        {}

resource "azurerm_key_vault_secret" "kv_value1" {
  key_vault_id = var.key_vault_id
  name         = replace(var.label, "/\\W/", "-")   ## tranform label
  value        = var.value
}

## configure access role on secret
resource "azurerm_role_assignment" "kv_role1" {
  for_each             = var.acls
  role_definition_name = var.role
  scope                = azurerm_key_vault_secret.kv_value1.resource_versionless_id
  principal_id         = each.value.object_id
}


