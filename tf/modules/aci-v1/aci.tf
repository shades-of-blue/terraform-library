#

data "azurerm_key_vault_secret" "container_secret" {
  count = length(var.containers[*].secure_environment_variables_from_key_vault) ? 1 : 0
  for_each = merge([for container_name, container in var.containers :
    {
      for k, v in container.secure_environment_variables_from_key_vault : "${container_name}/${k}" => {
        key_vault_id = v.key_vault_id
        name         = v.name
      }
    }
  ]...)
  key_vault_id = each.value.key_vault_id
  name         = each.value.name
}

data "azurerm_key_vault_secret" "volume_secret" {
  count = length(var.containers[*].secret_from_key_vault) ? 1 : 0
  for_each = local.secrets_from_volumes
  key_vault_id = each.value.key_vault_id
  name         = each.value.name
}

resource "azurerm_container_group" "azcg" {
  name                = var.container_group_name
  resource_group_name = var.resource_group_name
  ip_address_type     = var.ip_address_type        # Defaults to Public
  os_type             = var.os_type                # Defaults to Linux
  location            = var.location               # Defaults to westeurope
  restart_policy      = var.restart_policy         # Defaults to Always

  dynamic container {
    for_each = var.containers
    content {
      name                         = try(container.value.name, container.key)
      image                        = container.value.image
      cpu                          = container.value.cpu
      memory                       = container.value.memory
      environment_variables        = container.value.environment_variables
      secure_environment_variables = merge(
        {
          for variable_name, variable in container.value.secure_environment_variables_from_key_vault : variable_name =>
          data.azurerm_key_vault_secret.container_secret[format("%s/%s", container.key, variable_name)].value
        },
        container.value.secure_environment_variables
      )
      commands                     = container.value.commands
      dynamic ports {
        for_each = container.value.ports
        content {
          port = ports.value.port
          protocol = ports.value.protocol
        }
      }
      dynamic volume {
        for_each = container.value.volumes
        content {
          name = volume.key
          mount_path = volume.value.mount_path
          read_only = volume.value.read_only
          empty_dir = volume.value.empty_dir
          storage_account_name = volume.value.storage_account_name
          storage_account_key = volume.value.storage_account_key
          share_name = volume.value.share_name
          secret = merge(volume.value.secret, { for k, v in volume.value.secret_from_key_vault:
            k => base64encode(
              data.azurerm_key_vault_secret.volume_secret["${container.key}/${volume.key}/${v.name}"].value
            )
          })
          dynamic "git_repo" {
            for_each = volume.value.git_repo != null ? [volume.value.git_repo]: []
            content {
              url       = git_repo.value.url
              directory = git_repo.value.directory
              revision  = git_repo.value.revision
            }
          }
        }
      }
    }
  }

  dynamic "exposed_port" {
    for_each = var.exposed_ports
    content {
      port     = exposed_port.value.port
      protocol = exposed_port.value.protocol
    }
  }

  dynamic "dns_config" {
    for_each = var.dns_config
    content {
      nameservers = dns_config.value.nameservers
      search_domains = dns_config.value.search_domains
      options = dns_config.value.options
    }
  }

  dynamic "image_registry_credential" {
    for_each = var.image_registry_credential
    iterator = creds
    content {
      server   = creds.value.server
      username = creds.value.username
      password = creds.value.password
    }
  }
}


