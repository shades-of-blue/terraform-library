locals {
  secrets_from_volumes = merge([for container_name, container in var.containers : merge([
    for volume_name, volume in container.volumes : {
      for secret in volume.secret_from_key_vault : "${container_name}/${volume_name}/${secret.name}" => {
        key_vault_id = secret.key_vault_id
        name         = secret.name
      } if secret != {}
    }
    ]...)
  ]...)
}
