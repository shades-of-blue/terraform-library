#

variable "location" {
  description = "AZ deployment location."
  type        = string
  default     = "westeurope"
}

variable "container_group_name" {
  description = "Name of the container group"
  type        = string
}

variable "resource_group_name" {
  description = "Name of the resource group"
  type        = string
}

variable "os_type" {
  description = "Container OS. Possible values are Linux and Windows. Note that not all functions are supported on Windows."
  type        = string
  default     = "Linux"
}

variable "ip_address_type" {
  description = "The IP address type of the container (Public, Private or None). If set to Private, subnet_ids also needs to be set"
  type        = string
  default     = "Public"
}

variable "subnet_ids" {
  description = "The subnet resource IDs for a container group."
  type        = list(string)
  default     = []
}

variable "restart_policy" {
  description = "Restart policy for the container group. Allowed values are `Always`, `Never`, `OnFailure`."
  type        = string
  default     = "Always"
}

variable "exposed_ports" {
  description = "It can only contain ports that are also exposed on one or more containers in the group"
  type = list(object({
    port     = number
    protocol = optional(string, "TCP")
  }))
  default = []
}

variable "dns_name_label" {
  description = "The DNS label/name for the container group's IP. If not provided it will use the name of the resource"
  type        = string
  default     = null
}

variable "dns_config" {
  description = "Container resolver configuration"
  type = object(
    nameservers = list(string)
    search_domains = optional(list(string), [])
    options = optional(list(string), [])
  )
  default = {}
}


# Regarding volumes:
# Exactly one of these must be specified: empty_dir, git_repo, secret, or
# storage_account (all of: share_name, storage_account_name, storage_account_key) must be specified.
#
variable "containers" {
  description = "List of containers that will be running in the container group"
  type = map(object({
    name   = optional(string)
    image  = string
    cpu    = optional(number, 1)
    memory = optional(number, 2)
    ports  = optional(list(object({
      port     = number
      protocol = optional(string, "TCP")
    })), [])
    commands                     = optional(list(string), [])
    environment_variables        = optional(map(string), {})
    secure_environment_variables = optional(map(string), {})
    secure_environment_variables_from_key_vault = optional(map(object({
      key_vault_id = string
      name         = string
    })), {})
    volumes = optional(map(object({
      mount_path = string
      read_only  = optional(bool, false)
      empty_dir  = optional(bool, false)
      git_repo = optional(object({
        url       = string
        directory = optional(string)
        revision  = optional(string)
      }))
      secret = optional(map(string))
      secret_from_key_vault = optional(map(object({
        key_vault_id = string
        name         = string
      })), {})
      storage_account_name = optional(string)
      storage_account_key  = optional(string)
      share_name           = optional(string)
    })), {})
  }))

  validation {
    condition = alltrue(flatten([
      for container in var.containers : [
        for volume in container.volumes :
        (length([
          for v in [merge(volume.secret, volume.secret_from_key_vault), volume.storage_account_name, volume.git_repo, volume.empty_dir] : v
          if v != null && v != {}
        ]) == 1)
      ]
    ]))
    error_message = "Exactly one of empty_dir volume, git_repo volume, secret volume (secret or secret_from_key_vault) or storage account volume (share_name, storage_account_name, and storage_account_key) must be specified"
  }
}


variable "image_registry_credential" {
  description = "Credentials for ACR"
  type = list(object({
    server                    = string
    username                  = optional(string, null)
    password                  = optional(string, null)
    user_assigned_identity_id = optional(string, null)
  }))
  default = []
}

variable "zones" {
  description = "A list of availability zones in which the container group is located"
  type        = list(string)
  default     = []
}

