variable "display_names" { type = list(string) }

data "azuread_service_principal" "objects" {
  for_each     = toset(var.display_names)
  display_name = each.key
}

output "objectids" {
  value = [ for v in data.azuread_service_principal.objects: v.object_id ]
}

output "applicationids" {
  value = [ for v in data.azuread_service_principal.objects: v.application_id ]
}

output "objects" {
  value = data.azuread_service_principal.objects
}
