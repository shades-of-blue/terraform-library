## AD functions

This folder is the base if a collection of modules that behave as functions, i.e. each one of these modules have input variables and one or more output sections but no side-effects in terms of resource declarations.

Unfortunately, terraform currently has no support for custom functions and this module thing is as close as we can get and so there's no escaping the cumbersome consequences of having to use module declarations in stead of writing pure function calls.

#### Azure/AD function modules

  - fetchusers:     lookup user objects by principal name
  - fetchgroups:    lookup group objects by display name
  - fetchapps:      lookup applications  by display name
  - fetchpricipals: lookup service principals (including 'enterprise applcaitions') by display name

All functions have terraform outputs:

  - objects:    a list of objects (in HCL) representing the AD objects that match the input variables
  - objectids:  a list of object identifiers (GUIDs) matching the input variables


### Example: fetchusers

This module fetches user account information from Azure/AD. Input is a list of user principal names and output is a list of user object IDs. Call as follows:

module "fetchusers" {
  source = "../../lib/tf/modules/adfunctions/fetchusers"
  user_principal_names = [ 'sysop@testeurnl.onmicrosoft.com' ]
}

locals {
  sysop_id = module.fetchusers.object_id
}
