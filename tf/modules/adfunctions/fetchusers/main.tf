variable "user_principal_names" { type = list(string) }

data "azuread_user" "objects" {
  for_each            = toset(var.user_principal_names)
  user_principal_name = each.key
}

output "objectids" {
  value = [ for v in data.azuread_user.objects: v.object_id ]
}

output "objects" {
  value = data.azuread_user.objects
}
