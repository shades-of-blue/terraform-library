variable "display_names" { type = list(string) }

data "azuread_group" "objects" {
  for_each     = toset(var.display_names)
  display_name = each.key
}

output "objectids" {
  value = [ for v in data.azuread_group.objects: v.object_id ]
}

output "objects" {
  value = data.azuread_group.objects
}
