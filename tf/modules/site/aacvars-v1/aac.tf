#
## Fetch all standard AAC variables provided by the LZ deployment process.
## This module is very site-specific; it expects a global configuration object
## and a unit name as inputs and has default resource names that rely on
## particular naming conventions.
##
## Output is an object mapping AAC variable names to values.
#

variable "unitname" {}
variable "aac_name" { default = null }
variable "resource_group_name" { default = null }
variable "subscription_id" { default = null }
variable "aac_variables" {
  type    = set(string)
  default = null
}

locals {
  aac_name        = coalesce(var.aac_name, "aac-${var.unitname}")
  rg_name         = coalesce(var.resource_group_name,"rg-${var.unitname}-infra")
  subscription_id = coalesce(var.subscription_id,
                             data.azurerm_client_config.current.subscription_id)
  aac_variables   = coalesce(var.aac_variables, toset([
    "uniqid3",
    "uniqid8",
    "keyvault1_id",
    "keyvault1_name",
    "storageaccount1_id",
    "storageaccount1_name",
    "serviceaccount1_application_id",
    "serviceaccount1_principal_id",
    "accessgroup1_object_id",
    "subscription_resource_id"
  ]))
}

data "azurerm_client_config" "current" {}

#
## Fetch IDs of standard components provided by platform LZ deployment
## Not all of these entries may be present, depending on the landingzone
## configuration. If not present, referring to the value field of the aac
## variable lookup will yield a null value.
#

#
## The AAC object is provided as a convenience in case the caller wants
## to add additional variables (or other automation elements) to it.
#
data "azurerm_automation_account" "aac" {
  name                = local.aac_name
  resource_group_name = local.rg_name
}

## Fetch regular values
data "azurerm_automation_variable_string" "aac_values" {
  for_each                = local.aac_variables
  name                    = "platform.lz.${var.unitname}.${each.key}"
  automation_account_name = local.aac_name
  resource_group_name     = local.rg_name
}

## subscription_id is fetched separately to apply a postcondition
data "azurerm_automation_variable_string" "aac_subid" {
  name                    = "platform.lz.${var.unitname}.subscription_id"
  automation_account_name = local.aac_name
  resource_group_name     = local.rg_name
  lifecycle {
    postcondition {
      condition = self.value == local.subscription_id
      error_message = "Lookup failed: subscription ID mismatch"
    }
  }
}

output "aac" {
  value = merge({
    for v in local.aac_variables:
      (v) => data.azurerm_automation_variable_string.aac_values[v].value
  },{
    subscription_id = data.azurerm_automation_variable_string.aac_subid.value
    aac_object      = data.azurerm_automation_account.aac
  })
}
