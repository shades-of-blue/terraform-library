## This module needs access to the platform subscription
terraform {
  required_providers {
    azurerm = {
      configuration_aliases = [
        azurerm.platformsub
      ]
    }
  }
}
