#
## Module gvm+dns: declare a generic virtual machine and then add DNS A records
## for private addresses to the private DNS zone indicated in the VM parameters.
##
## Most of the parameters are passed to the generic vm module. Two of them are
## also used here:
##
##     dns_resource_group_name   - platform resource group where pDNS zones are
##     args.dns_zone_name        - platform pDNS zone where the VM A records are in
#
variable "name" {}
variable "args" {}

variable "location" {}
variable "resource_group_name" {}
variable "network_resource_group_name" {}
variable "dns_resource_group_name" { default = null }
variable "virtual_network_name" {}
variable "admin_password" { default = null }

## Pass everything to generic vm module
module "gvm" {
  source                      = "../../gvm-v1"
  #providers = {
  #  azurerm = azurerm
  #}
  name                        = var.name
  args                        = var.args
  location                    = var.location
  resource_group_name         = var.resource_group_name
  network_resource_group_name = var.network_resource_group_name
  virtual_network_name        = var.virtual_network_name
  admin_password              = var.admin_password
}

## Configure a DNS A record for all private IP addresses of this VM
resource "azurerm_private_dns_a_record" "arecord" {
  count               = try(var.args.dns_zone_name != null,false) ? 1 : 0
  provider            = azurerm.platformsub
  name                = var.name
  zone_name           = try(var.args.dns_zone_name,null)
  resource_group_name = local.zone_resource_group_name
  ttl                 = local.ttl
  records             = local.ipaddrs
}

locals {
  ## platform DNS resource group:
  zone_resource_group_name = coalesce(var.dns_resource_group_name,"rg-dns")
  ttl                      = try(var.args.dns_record_ttl,1800)
  ipaddrs                  = flatten([
      for i in module.gvm.nics:
        i.ip_configuration[*].private_ip_address
  ])
}

#
## Pass through gvm outputs
#
output "os_disk_id" {
  value = module.gvm.os_disk_id
}

output "nics" {
  value = module.gvm.nics
}

output "vm" {
  value = module.gvm.vm
}
