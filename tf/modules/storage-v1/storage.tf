#
## Azure Storage account module
## This module declares a storage account resource with zero or more optional
## associated resources:
##
##   storage containers (blobs)
##   file shares
##   private network connections
#

## Variables: name, location of storage account and various parameters in map 'args'
variable "location" {}
variable "name" {}
variable "args" {}

## Variables: context and global configuration items
variable "uniqid3" {}
variable "resource_group_name" {}
variable "network_resource_group_name" {}
variable "virtual_network_name" {}
variable "pdnszone_ids" {}
variable "backup_configuration_data" { default = {} }
variable "keyvault_id" { default = {} }
variable "tenant_id" { default = {} }

## Optional subnet for private links
data "azurerm_subnet" "sn" {
  count                = try(var.args.subnet_name != null,false) ? 1 : 0
  resource_group_name  = var.network_resource_group_name
  virtual_network_name = var.virtual_network_name
  name                 = var.args.subnet_name
}

resource "azurerm_storage_account" "storage_account" {
  name                = "st${replace(var.name,"/\\W/","")}${var.uniqid3}"
  location            = var.location
  resource_group_name = var.resource_group_name
  tags                = try(var.args.tags,null)

  account_tier                    = try(var.args.account_tier,null)
  account_replication_type        = try(var.args.account_replication_type,null)
  access_tier                     = try(var.args.access_tier,null)
  account_kind                    = try(var.args.account_kind,null)
  enable_https_traffic_only       = try(var.args.enable_https_traffic_only,null)
  min_tls_version                 = try(var.args.min_tls_version,null)

  large_file_share_enabled        = try(var.args.large_file_share_enabled,null)
  shared_access_key_enabled       = try(var.args.shared_access_key_enabled,null)
  public_network_access_enabled   = try(var.args.public_network_access_enabled,null)
  default_to_oauth_authentication = try(var.args.default_to_oauth_authentication,null)

  identity {
    type         = try(var.args.identity.type,"SystemAssigned")
    identity_ids = try(var.args.identity.identity_ids,null)
  }

  dynamic "custom_domain" {
    for_each = try(var.args.custom_domain,{})
    content {
      name          = try(custom_domain.value.name,custom_domain.key)
      use_subdomain = try(custom_domain.value.use_subdomain,null)
    }
  }

  ## azure_files_authentication: optional single block
  dynamic "azure_files_authentication" {
    for_each = try([var.args.azure_files_authentication],{})
    content {
      directory_type = try(azure_files_authentication.value.directory_type,null)
      dynamic "active_directory" {
        for_each = try([azure_files_authentication.value.active_directory],{})
        iterator = ad
        content {
          domain_name         = try(ad.value.domain_name,null)
          domain_sid          = try(ad.value.domain_sid,null)
          domain_guid         = try(ad.value.domain_guid,null)
          forest_name         = try(ad.value.forest_name,null)
          netbios_domain_name = try(ad.value.netbios_domain_name,null)
          storage_sid         = try(ad.value.storage_sid,null)
        }
      }
    }
  }

  dynamic "routing" {
    for_each = try(var.args.routing,{})
    content {
      choice                      = try(routing.value.choice,null)
      publish_internet_endpoints  = try(routing.value.publish_internet_endpoints,null)
      publish_microsoft_endpoints = try(routing.value.publish_microsoft_endpoints,null)
    }
  }

  dynamic "network_rules" {
    for_each = try(var.args.network_rules,{})
    content {
      default_action             = try(network_rules.value.default_action,null)
      bypass                     = try(network_rules.value.bypass,null)
      ip_rules                   = try(network_rules.value.ip_rules,[])
      virtual_network_subnet_ids = try(network_rules.value.virtual_network_subnet_ids,[])
      dynamic "private_link_access" {
        for_each = try(network_rules.value.private_link_access,{})
        content {
          endpoint_resource_id = try(private_link_access.value.endpoint_resource_id,null)
          endpoint_tenant_id   = try(private_link_access.value.endpoint_tenant_id,null)
        }
      }
    }
  }

  ## share_properties: optional single block
  dynamic "share_properties" {
    for_each = try([var.args.share_properties],{})
    content {
      ## cors_rule: optional single block
      dynamic "cors_rule" {
        for_each = try([share_properties.value.cors_rule],{})
        content {
          allowed_headers    = try(cors_rule.value.allowed_headers,null)
          allowed_methods    = try(cors_rule.value.allowed_methods,null)
          allowed_origins    = try(cors_rule.value.allowed_origins,null)
          exposed_headers    = try(cors_rule.value.exposed_headers,null)
          max_age_in_seconds = try(cors_rule.value.max_age_in_seconds,null)
        }
      }

      ## retention_policy: optional single block
      dynamic "retention_policy" {
        for_each = try([share_properties.value.retention_policy],{})
        content {
          days = try(retention_policy.value.days,null)
        }
      }

      ## smb: optional single block
      dynamic "smb" {
        for_each = try([share_properties.value.smb],{})
        content {
          versions                        = try(smb.value.versions,null)
          authentication_types            = try(smb.value.authentication_types,null)
          kerberos_ticket_encryption_type = try(smb.value.kerberos_ticket_encryption_type,null)
          channel_encryption_type         = try(smb.value.channel_encryption_type,null)
          multichannel_enabled            = try(smb.value.multichannel_enabled,null)
        }
      }
    }
  }

  ## blob: optional single block
  dynamic "blob_properties" {
    for_each = try([var.args.blob_properties],{})
    content {
      versioning_enabled  = try(blob_properties.value.versioning_enabled,null)
      change_feed_enabled  = try(blob_properties.value.change_feed_enabled,null)
      change_feed_retention_in_days  = try(blob_properties.value.change_feed_retention_in_days,null)
      default_service_version  = try(blob_properties.value.default_service_version,null)
      last_access_time_enabled  = try(blob_properties.value.last_access_time_enabled,null)

      ## cors_rule: optional single block
      dynamic "cors_rule" {
        for_each = try([blob_properties.value.cors_rule],{})
        content {
          allowed_headers    = try(cors_rule.value.allowed_headers,null)
          allowed_methods    = try(cors_rule.value.allowed_methods,null)
          allowed_origins    = try(cors_rule.value.allowed_origins,null)
          exposed_headers    = try(cors_rule.value.exposed_headers,null)
          max_age_in_seconds = try(cors_rule.value.max_age_in_seconds,null)
        }
      }

      ## delete_retention_policy: optional single block
      dynamic "delete_retention_policy" {
        for_each = try([blob_properties.value.delete_retention_policy],{})
        content {
          days = try(delete_retention_policy.value.days,null)
        }
      }

      ## restore_policy: optional single block
      dynamic "restore_policy" {
        for_each = try([blob_properties.value.restore_policy],{})
        content {
          days = try(restore_policy.value.days,null)
        }
      }

      ## container_delete_retention_policy: optional single block
      dynamic "container_delete_retention_policy" {
        for_each = try([blob_properties.value.container_delete_retention_policy],{})
        content {
          days = try(container_delete_retention_policy.value.days,null)
        }
      }

    }
  }

  ## queue: optional single block
  dynamic "queue_properties" {
    for_each = try([var.args.queue_properties],{})
    content {
      ## cors_rule: optional single block
      dynamic "cors_rule" {
        for_each = try([queue_properties.value.cors_rule],{})
        content {
          allowed_headers    = try(cors_rule.value.allowed_headers,null)
          allowed_methods    = try(cors_rule.value.allowed_methods,null)
          allowed_origins    = try(cors_rule.value.allowed_origins,null)
          exposed_headers    = try(cors_rule.value.exposed_headers,null)
          max_age_in_seconds = try(cors_rule.value.max_age_in_seconds,null)
        }
      }

      ## logging: optional single block
      dynamic "logging" {
        for_each = try([queue_properties.value.logging],{})
        content {
          delete                = try(logging.value.delete,null)
          read                  = try(logging.value.read,null)
          version               = try(logging.value.version,null)
          write                 = try(logging.value.write,null)
          retention_policy_days = try(logging.value.retention_policy_days,null)
        }
      }

      ## minute_metrics: optional single block
      dynamic "minute_metrics" {
        for_each = try([queue_properties.value.minute_metrics],{})
        content {
          enabled               = try(minute_metrics.value.enabled,null)
          version               = try(minute_metrics.value.version,null)
          include_apis          = try(minute_metrics.value.include_apis,null)
          retention_policy_days = try(minute_metrics.value.retention_policy_days,null)
        }
      }

      ## hour_metrics: optional single block
      dynamic "hour_metrics" {
        for_each = try([queue_properties.value.hour_metrics],{})
        content {
          enabled               = try(hour_metrics.value.enabled,null)
          version               = try(hour_metrics.value.version,null)
          include_apis          = try(hour_metrics.value.include_apis,null)
          retention_policy_days = try(hour_metrics.value.retention_policy_days,null)
        }
      }

    }
  }

  lifecycle {
    ignore_changes = [
      customer_managed_key
    ]
  }

}

#
## Optional Storage blobs
#
resource "azurerm_storage_container" "containers" {
  for_each              = try(var.args.containers,{})
  name                  = each.key
  storage_account_name  = azurerm_storage_account.storage_account.name
  container_access_type = try(each.value.container_access_type,null)
  metadata              = try(each.value.metadata,null)
}

#
## Optional File shares
#
resource "azurerm_storage_share" "shares" {
  for_each              = try(var.args.shares,{})
  name                  = each.key
  storage_account_name  = azurerm_storage_account.storage_account.name
  access_tier           = try(each.value.access_tier,null)
  enabled_protocol      = try(each.value.enabled_protocol,null)

  quota                 = try(each.value.quota,null)
  metadata              = try(each.value.metadata,null)

  ## acl: 0* block
  dynamic "acl" {
    for_each = try(each.value.acl,{})
    content {
      id            = try(acl.value.id,acl.key)
      ## access_policy: optional single block
      dynamic "access_policy" {
        for_each = try([acl.value.access_policy],{})
        content {
          permissions = try(access_policy.value.permissions,null)
          start       = try(access_policy.value.start,null)
          expiry      = try(access_policy.value.expiry,null)
        }
      }
    }
  }

  lifecycle {
    ignore_changes = [
      acl
    ]
  }

}

#
## Configure a backup policy for each file share for which it is configured
#
resource "azurerm_backup_policy_file_share" "backup_policies" {
  for_each            = {
    for k,v in try(var.args.shares,{}): (k) => v if (
        can(v.backup_policy) && try(var.args.enable_backup,false) )
  }

  name                = "${var.name}-${each.key}-backup-policy"
  resource_group_name = var.backup_configuration_data.resource_group_name
  recovery_vault_name = var.backup_configuration_data.recovery_vault_name

  backup {
    frequency = each.value.backup_policy.frequency
    time      = each.value.backup_policy.time
  }

  retention_daily {
    count = each.value.backup_policy.retention_daily.count
  }

  dynamic "retention_weekly" {
    for_each = try([each.value.backup_policy.retention_weekly],{})
    content {
      count    = retention_weekly.value.count
      weekdays = retention_weekly.value.weekdays
    }
  }

  dynamic "retention_monthly" {
    for_each = try([each.value.backup_policy.retention_monthly],{})
    content {
      count    = retention_monthly.value.count
      weekdays = retention_monthly.value.weekdays
      weeks    = retention_monthly.value.weeks
    }
  }

  dynamic "retention_yearly" {
    for_each = try([each.value.backup_policy.retention_yearly],{})
    content {
      count    = retention_yearly.value.count
      weekdays = retention_yearly.value.weekdays
      weeks    = retention_yearly.value.weeks
      months   = retention_yearly.value.months
    }
  }

}

## Attach a backup policy to a file share
resource "azurerm_backup_protected_file_share" "share1" {
  for_each                  = {
    for k,v in try(var.args.shares,{}): (k) => v if (
        can(v.backup_policy) && try(var.args.enable_backup,false) )
  }
  resource_group_name       = var.backup_configuration_data.resource_group_name
  recovery_vault_name       = var.backup_configuration_data.recovery_vault_name

  source_storage_account_id = azurerm_storage_account.storage_account.id
  source_file_share_name    = each.key
  backup_policy_id          = azurerm_backup_policy_file_share.backup_policies[each.key].id
}

## Register storage account with recovery vault, if configured
resource "azurerm_backup_container_storage_account" "backup_container" {
  count               = try(var.args.enable_backup,false) ? 1 : 0
  resource_group_name = var.backup_configuration_data.resource_group_name
  recovery_vault_name = var.backup_configuration_data.recovery_vault_name
  storage_account_id  = azurerm_storage_account.storage_account.id
}

#
## Optional private network endpoints
#
resource "azurerm_private_endpoint" "storage_private_endpoints" {
  for_each                      = try(var.args.private_endpoints,{})
  name                          = "${var.name}-${each.key}"
  location                      = var.location
  resource_group_name           = var.resource_group_name
  subnet_id                     = data.azurerm_subnet.sn[0].id
  custom_network_interface_name = "${var.name}-${each.key}-nic"

  ## make the service register its DNS records in our private zone table
  private_dns_zone_group {
    name                 = "plookups"
    private_dns_zone_ids = [ var.pdnszone_ids[each.value.pdnskey] ]
  }

  ## link this private endpoint to the service that should use it
  private_service_connection {
    name                           = "${var.name}-${each.key}-cn"
    private_connection_resource_id = azurerm_storage_account.storage_account.id
    is_manual_connection           = false
    subresource_names              = each.value.subresource_names
  }
}

#
## Optional: customer key management.
## Parameters passed in args.customer_keydata: keyvault id, key parameters
#
locals {
  do_custom_key = can(var.args.customer_keydata)
  keyname       = "k${replace(var.name,"/\\W/","")}${var.uniqid3}"
}

## Assign key to storage account
resource "azurerm_storage_account_customer_managed_key" "customer_key" {
  count              = local.do_custom_key ? 1 : 0
  storage_account_id = azurerm_storage_account.storage_account.id
  key_vault_id       = var.keyvault_id
  key_name           = local.keyname
}

## Maintain key in vault
resource "azurerm_key_vault_key" "customer_key" {
  count        = local.do_custom_key ? 1 : 0
  name         = local.keyname
  key_vault_id = var.keyvault_id
  key_type     = try(var.args.customer_keydata.key_type, "RSA")
  key_size     = try(var.args.customer_keydata.key_size, null)
  curve        = try(var.args.customer_keydata.curve, null)
  key_opts     = [
    "decrypt",
    "encrypt",
    "sign",
    "verify",
    "unwrapKey",
    "wrapKey"
  ]

  dynamic "rotation_policy" {
    for_each = try([var.args.customer_keydata.rotation_policy],{})
    content {
      expire_after         = try(rotation_policy.value.expire_after,null)
      notify_before_expiry = try(rotation_policy.value.notify_before_expiry,null)
      dynamic "automatic" {
        for_each = try([rotation_policy.value.automatic],{})
        content {
          time_after_creation = try(automatic.value.time_after_creation,null)
          time_before_expiry  = try(automatic.value.time_before_expiry,null)
        }
      }
    }
  }

}

## Assign access to storage account identity
resource "azurerm_key_vault_access_policy" "storage_principal" {
  count        = local.do_custom_key ? 1 : 0
  key_vault_id = var.keyvault_id
  tenant_id    = var.tenant_id
  object_id    = azurerm_storage_account.storage_account.identity.0.principal_id

  secret_permissions = ["Get"]
  key_permissions = [
    "Get",
    "UnwrapKey",
    "WrapKey"
  ]
}

resource "azurerm_advanced_threat_protection" "storprot1" {
  count              = can(var.args.enable_threat_protection) ? 1 : 0
  target_resource_id = azurerm_storage_account.storage_account.id
  enabled            = var.args.enable_threat_protection
}

#
## Output section: export storage-related resources whole-sale.
#
output "storage_account" { value = azurerm_storage_account.storage_account }
output "containers" { value = azurerm_storage_container.containers }
output "shares" { value = azurerm_storage_share.shares }
