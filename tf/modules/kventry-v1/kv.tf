#
## Store secret in key vault and arrange access to list of reader principals.
## Parameters:
##  secret_value         - the secret to store
##  kvid                 - key vault identifier to store secret in
##  principal_name       - principal associated with secret
##  acl_list             - principals that get read access
##                         The entries in this list are tagged:
##                          'upn:' for user principal names
##                          'grp:' for group display names
##                          'svc:' for service principal display names
##
## Principals in the acl list may be retrieve secrets by name, for instance
## with powershell:
##  $secret = Get-AzKeyVaultSecret -VaultName "kvaadstage" -Name "azuread-principal-sa-hobgoblin-eur-edu" -AsPlainText
##
## with AZ CLI:
##
## az keyvault secret show --vault-name "kvaadstage" --name "azuread-principal-sa-hobgoblin-eur-edu" --query "value"
##
#
variable "secret" {}
variable "kvid" {}
variable "principal_name" {}
variable "acl_list" {
  default = []
  nullable = true
}

locals {
  kvname = "azuread-principal-${replace(var.principal_name, "/\\W/", "-")}"
  kv_secret_role_name = "Key Vault Secrets User"

  ## split acl name list by tag
  acl_unames = flatten([
    for p in try(var.acl_list,[]):
      regexall("upn:(.*)", p)
  ])
  acl_gnames = flatten([
    for p in try(var.acl_list,[]):
      regexall("grp:(.*)", p)
  ])
  acl_snames = flatten([
    for p in try(var.acl_list,[]):
      regexall("svc:(.*)", p)
  ])

  ## lookup azure/ad objects for each tag
  acl_uobjs = {
    for u in local.acl_unames:
      u => data.azuread_user.adobject[u]
  }
  acl_gobjs = {
    for g in local.acl_gnames:
      g => data.azuread_group.adobject[g]
  }
  acl_sobjs = {
    for s in local.acl_snames:
      s => data.azuread_service_principal.adobject[s]
  }
}

resource "azurerm_key_vault_secret" "kv_secret1" {
  count        = (var.kvid != null) ? 1 : 0
  key_vault_id = var.kvid
  name         = local.kvname
  value        = var.secret
}

## configure access role on secret
resource "azurerm_role_assignment" "kv_role1" {
  for_each             = merge(local.acl_uobjs,local.acl_gobjs,local.acl_sobjs)
  scope                = azurerm_key_vault_secret.kv_secret1.0.resource_versionless_id
  role_definition_name = local.kv_secret_role_name
  principal_id         = each.value.object_id
}

## Lookup user and group objects
data "azuread_user" "adobject" {
  for_each            = { for u in local.acl_unames: u => {} }
  user_principal_name = each.key
}

data "azuread_group" "adobject" {
  for_each         = { for g in local.acl_gnames: g => {} }
  display_name     = each.key
  security_enabled = true
}

data "azuread_service_principal" "adobject" {
  for_each         = { for s in local.acl_snames: s => {} }
  display_name     = each.key
}
