#
## Module vmcomplex: declare a virtual machine including a network interface
## and optional azure vm extensions.
#
variable "location" {}
variable "name" {}
variable "args" {}

variable "resource_group_name" {}
variable "network_resource_group_name" {}
variable "virtual_network_name" {}
variable "admin_password" { default = null }

locals {
  ## Prepare the default nic/ip_configuration section
  nic_ip_configuration_dflt = {
    for k,v in var.args.network_interfaces:
      k => {
        name                          = k
        subnet_id                     = data.azurerm_subnet.sn[k].id
        private_ip_address_allocation = "Dynamic"
      }
  }
}

data "azurerm_subnet" "sn" {
  for_each             = var.args.network_interfaces
  resource_group_name  = var.network_resource_group_name
  virtual_network_name = var.virtual_network_name
  name                 = each.value.subnet_name
}

## Declare a network interface for each vm
resource "azurerm_network_interface" "nics" {
  for_each            = var.args.network_interfaces
  name                = "${var.name}-${each.key}"
  location            = var.location
  resource_group_name = var.resource_group_name

  #
  ## ip configuration: split into "main" and "extra" sections...
  ## Restrictions apply: only one subnet name lookup per interface
  #
  dynamic "ip_configuration" {
    for_each = [
      merge(
        local.nic_ip_configuration_dflt[each.key],
        try(each.value.ip_configuration,{})
      )
    ]
    content {
      name                          = try(ip_configuration.value.name,null)
      subnet_id                     = try(ip_configuration.value.subnet_id,null)
      private_ip_address_allocation = try(ip_configuration.value.private_ip_address_allocation,null)
    }
  }

  dynamic "ip_configuration" {
    for_each = try(each.value.secondary_ip_configurations,{})
    content {
      name                          = try(ip_configuration.value.name,null)
      subnet_id                     = try(ip_configuration.value.subnet_id,null)
      private_ip_address_allocation = try(ip_configuration.value.private_ip_address_allocation,null)
    }
  }

}

resource "azurerm_windows_virtual_machine" "vm" {
  name                = var.name
  location            = var.location
  resource_group_name = var.resource_group_name

  size                  = var.args.size
  license_type          = try(var.args.license_type,null)
  #network_interface_ids = azurerm_network_interface.nics[*].id
  network_interface_ids = [ for i in azurerm_network_interface.nics: i.id ]
  computer_name         = try(var.args.computer_name,null)
  custom_data           = try(var.args.custom_data,null)

  admin_username        = try(var.args.admin_username,null)
  admin_password        = coalesce(var.admin_password,var.args.admin_password)

  identity {
    type         = try(var.args.identity.type,"SystemAssigned")
    identity_ids = try(var.args.identity.identity_ids,null)
  }

  ## requred single os_disk
  os_disk {
    #name                 = try(var.args.os_disk.name,"${var.name}-osdisk")
    caching              = try(var.args.os_disk.caching,null)
    storage_account_type = try(var.args.os_disk.storage_account_type,null)
    disk_size_gb         = try(var.args.os_disk.disk_size_gb,null)
  }

  dynamic "boot_diagnostics" {
    for_each = try(var.args.boot_diagnostics,{})
    content {
      storage_account_uri = try(var.args.boot_diagnostics.storage_account_uri,null)
    }
  }

  dynamic "secret" {
    for_each = try(var.args.secrets,{})
    content {
      key_vault_id = try(secret.value.key_vault_id,secret.key)
      dynamic "certificate" {
        for_each = try(secret.value.certificates,{})
        content {
          url   = try(certificate.value.url,null)
          store = try(certificate.value.store,null)
        }
      }

    }
  }

  priority = try(var.args.priority,null)
  eviction_policy = try(var.args.eviction_policy,null)
  max_bid_price = try(var.args.eviction_max_bid_price,null)

  provision_vm_agent = try(var.args.provision_vm_agent,null)
  patch_assessment_mode = try(var.args.patch_assessment_mode,null)
  patch_mode = try(var.args.patch_mode,null)

  ## optional source_image_reference
  dynamic "source_image_reference" {
    for_each = try([var.args.source_image_reference],{})
    content {
      publisher = try(source_image_reference.value.publisher,null)
      offer     = try(source_image_reference.value.offer,null)
      sku       = try(source_image_reference.value.sku,null)
      version   = try(source_image_reference.value.version,null)
    }
  }

  ## optional source_image_id
  source_image_id = try(var.args.source_image_id,null)

  ## optional plan
  dynamic "plan" {
    for_each = try([var.args.plan],{})
    content {
      name      = try(plan.value.name,null)
      product   = try(plan.value.product,null)
      publisher = try(plan.value.publisher,null)
    }
  }

  ## optional gallery_application
  dynamic "gallery_application" {
    for_each = try([var.args.gallery_application],{})
    content {
      version_id             = try(gallery_application.value.version_id,null)
      configuration_blob_uri = try(gallery_application.value.configuration_blob_uri,null)
      order                  = try(gallery_application.value.order,null)
      tag                    = try(gallery_application.value.tag,null)
    }
  }
}


resource "azurerm_virtual_machine_extension" "extensions" {
  for_each             = try(var.args.extensions,{})
  name                 = each.key
  virtual_machine_id   = azurerm_windows_virtual_machine.vm.id
  publisher            = each.value.publisher
  type                 = each.value.type
  type_handler_version = each.value.type_handler_version

  settings             = try(jsonencode(each.value.settings),null)
  protected_settings   = try(jsonencode(each.value.protected_settings),null)

  lifecycle {
    ignore_changes = [settings, protected_settings]
  }

  depends_on = []
}

output "vm" { value = azurerm_windows_virtual_machine.vm }
output "nics" { value = azurerm_network_interface.nics }


## output: data that identifies the OS disk for this virtual machine
data "azurerm_managed_disk" "osdisk" {
  name                = azurerm_windows_virtual_machine.vm.os_disk[0].name
  resource_group_name = var.resource_group_name
}
output "os_disk_id" { value = data.azurerm_managed_disk.osdisk.id }
