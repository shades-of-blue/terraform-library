#
## Module gvm: declare a virtual machine including a network interface
## and optional azure vm extensions. Since the terraform/azuread provider
## uses differently named resource for linux/windows virtual machines, we
## employ this shim to try to make life easier. However, it does mean that
## the VM configuration elements in the 'args' parameter must reflect the
## signature of the os-specific azurerm resource.
#

variable "name" {}
variable "args" {}

variable "location" {}
variable "resource_group_name" {}
variable "network_resource_group_name" {}
variable "virtual_network_name" {}
variable "admin_password" { default = null }

module "lvm" {
  count = try(var.args.vm_genre == "Linux", false) ? 1 : 0
  source = "./lvm"
  name = var.name
  args = var.args
  location = var.location
  resource_group_name = var.resource_group_name
  network_resource_group_name = var.network_resource_group_name
  virtual_network_name = var.virtual_network_name
  admin_password = var.admin_password
}

module "wvm" {
  count = try(var.args.vm_genre == "Windows", false) ? 1 : 0
  source = "./wvm"
  name = var.name
  args = var.args
  location = var.location
  resource_group_name = var.resource_group_name
  network_resource_group_name = var.network_resource_group_name
  virtual_network_name = var.virtual_network_name
  admin_password = var.admin_password
}


output "vm" {
  value = try(
    module.lvm[0].vm,
    module.wvm[0].vm,
    null
  )
}

output "nics" {
  value = try(
    module.lvm[0].nics,
    module.wvm[0].nics,
    null
  )
}

output "os_disk_id" {
  value = try(
    module.lvm[0].os_disk_id,
    module.wvm[0].os_disk_id,
    null
  )
}
