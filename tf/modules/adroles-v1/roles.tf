#
## Declare Azure/AD roles assignments for a principal.
##
## The principal may be a name or an id. If a name is specified it is looked
## for in Azure/AD by display name (group) or principal name (user)
#
variable "principal_id" {
  default = null
}

variable "user_principal_name" {
  default = null
}
variable "group_display_name" {
  default = null
}

variable "adroles" {
  type    = list
  default = []
}

locals {
  ## convert array to map
  mapped_adroles = { for r in var.adroles: r => {} }
  lookup = var.principal_id == null
  principal_id = coalesce(
    var.principal_id,
    try(data.azuread_group.group.0.object_id,null),
    try(data.azuread_user.user.0.object_id,null)
  )
}

## Activate roles in tenant
resource "azuread_directory_role" "adroles" {
  for_each     = local.mapped_adroles
  display_name = each.key
}

## Assign roles to principal
resource "azuread_directory_role_assignment" "assignment" {
  for_each            = local.mapped_adroles
  role_id             = azuread_directory_role.adroles[each.key].template_id
  principal_object_id = local.principal_id
}

data "azuread_user" "user" {
  count = var.user_principal_name != null ? 1 : 0
  user_principal_name = var.user_principal_name
}

data "azuread_group" "group" {
  count = var.group_display_name != null ? 1 : 0
  display_name = var.group_display_name
}

