#### Assign Azure RM roles

This module offers a shorthand way of assigning a set of roles to a collection of assignees.

Example call:

```
module "rmroles" {
  source     = "../../lib/tf/modules/rmroles-v1"
  scope      = local.resource_group.id
  roles      = [
     "Storage Blob Data Contributor",
     "Key Vault Secrets Reader",
  ]
  principals = [
     data.azuread_service_principal.deployer_account.object_id,
     data.azuread_user.application_admin_user.object_id
  ]
}

```
