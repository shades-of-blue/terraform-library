#
## Assign a set of azure rm roles to a collection of assignees in a given scope
#

variable "scope" {}
variable "roles" {}
variable "principals" {}

resource "azurerm_role_assignment" "role_assigment" {
  for_each             = {
    ## Construct a map of individual role assignments
    for pair in setproduct(var.roles,var.principals):
     "${pair[0]}x${pair[1]}" => {
        role      = pair[0]
        principal = pair[1]
      }
  }
  scope                = var.scope
  role_definition_name = each.value.role
  principal_id         = each.value.principal
}
