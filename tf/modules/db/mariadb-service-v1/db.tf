#
## Manage mariadb server and databases
#

locals {
  server_parameters = lookup(var.args, "server_parameters", {})
  dbs               = lookup(var.args, "databases", {})
}

## Generate a db server admin password (that optionally also goes in a key vault)
resource "random_password" "db_admin_password" {
  length           = var.admin_password_length
  special          = true
  override_special = ":"
}

#
## Configure mariadb service resource
## cf. https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/mariadb_database
#
resource "azurerm_mariadb_server" "mysqlService1" {
  name                          = try(var.args.dbname,"${var.name}-${var.uniqid}")
  location                      = var.location
  resource_group_name           = var.resource_group_name

  administrator_login           = var.admin_account
  administrator_login_password  = random_password.db_admin_password.result

  sku_name                      = var.args.sku_name
  version                       = var.args.version
  storage_mb                    = try(var.args.storage_mb,null)

  auto_grow_enabled             = try(var.args.auto_grow_enabled,null)
  backup_retention_days         = try(var.args.backup_retention_days,null)
  geo_redundant_backup_enabled  = try(var.args.geo_redundant_backup_enabled,null)
  ssl_enforcement_enabled       = try(var.args.ssl_enforcement_enabled,true)
  ssl_minimal_tls_version_enforced = try(var.args.ssl_minimal_tls_version_enforced,null)
  public_network_access_enabled = try(var.args.public_network_access_enabled,null)
}

## Store password in keyvault, if requested
resource "azurerm_key_vault_secret" "store-dbserver-password" {
  count        = (var.key_vault_id != "") ? 1 : 0
  name         = "${var.name}-dbserver-accesskey"
  value        = random_password.db_admin_password.result
  key_vault_id = var.key_vault_id
  #depends_on  = [azurerm_key_vault_access_policy.deployAccountAccess1]
}

## Manage db server parameters
resource "azurerm_mariadb_configuration" "server_parameters" {
  for_each            = local.server_parameters
  resource_group_name = var.resource_group_name
  server_name         = azurerm_mariadb_server.mysqlService1.name
  name                = each.key
  value               = each.value
}

## Configure databases on this service
resource "azurerm_mariadb_database" "databases" {
  for_each            = local.dbs
  name                = each.key
  resource_group_name = var.resource_group_name
  server_name         = azurerm_mariadb_server.mysqlService1.name
  charset             = lookup(each.value, "charset", "utf8")
  collation           = lookup(each.value, "collation", "utf8_general_ci")
}

#
## Optional private network endpoints
#
resource "azurerm_private_endpoint" "storage_private_endpoints" {
  count                         = var.private_endpoint_data == {} ? 0 : 1
  name                          = "${var.name}-mariadb"
  location                      = var.location
  resource_group_name           = var.resource_group_name
  subnet_id                     = data.azurerm_subnet.sn[0].id
  custom_network_interface_name = "${var.name}-mariadb-nic"

  ## make the service register its DNS records in our private zone table
  private_dns_zone_group {
    name                 = "plookups"
    private_dns_zone_ids = [ var.private_endpoint_data.private_dnszone_id ]
  }

  ## link this private endpoint to the service that should use it
  private_service_connection {
    name                           = "${var.name}-mariadb-cn"
    private_connection_resource_id = azurerm_mariadb_server.mysqlService1.id
    is_manual_connection           = false
    subresource_names              = var.private_endpoint_data.subresource_names
  }
}

## Optional subnet for private links
data "azurerm_subnet" "sn" {
  count                = var.private_endpoint_data == {} ? 0 : 1
  resource_group_name  = var.private_endpoint_data.network_resource_group_name
  virtual_network_name = var.private_endpoint_data.virtual_network_name
  name                 = var.private_endpoint_data.subnet_name
}

#
## Output section: export mariadb resources whole-sale.
#
output "dbserver"  { value = azurerm_mariadb_server.mysqlService1 }
output "databases" { value = azurerm_mariadb_database.databases }
output "db_admin_password" { value = random_password.db_admin_password.result }

