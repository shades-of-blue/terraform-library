variable "name"                  { }
variable "location"              { type = string }
variable "resource_group_name"   { type = string }
variable "key_vault_id"          { default = null }
variable "admin_account"         { default = "dbadmin" }
variable "admin_password_length" { default = 37 }

variable "uniqid"                { default = "" }
variable "private_endpoint_data" { default = {} }

variable "args"                  { }
/*
variable "args"                  {
  type = object({
    name                         = optional(string)
    sku_name                     = string
    version                      = string
    storage_mb                   = optional(number)
    auto_grow_enabled            = optional(bool)
    backup_retention_days        = optional(number)
    geo_redundant_backup_enabled = optional(bool)
    ssl_enforcement_enabled      = optional(bool)
  })
}
*/
