#
## Manage databases and server settings defined in input file (yaml format)
#

locals {
  db_data            = var.db_data

  server_parameters = lookup(local.db_data, "server_parameters", {})
  dbs               = lookup(local.db_data, "databases", {})
}

## Generate a db server admin password (that optionally also goes in a key vault)
resource "random_password" "db_admin_password" {
  length           = var.password_length
  special          = true
  override_special = ":"
}

#
## Configure mssql service resource
## cf. https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/mssql_server
#
resource "azurerm_mssql_server" "mServer1" {
  name                          = var.db_servicename
  location                      = var.location
  resource_group_name           = var.resource_group_name

  administrator_login           = var.admin_account
  administrator_login_password  = random_password.db_admin_password.result

  identity {
    type = "SystemAssigned"
  }
  dynamic "azuread_administrator" {
    for_each = try([local.db_data.azuread_administrator],{})
     iterator = aa
     content {
       login_username = try(aa.value.login_username,null)
       object_id = try(aa.value.object_id,null)
       tenant_id = try(aa.value.tenant_id,null)
       azuread_authentication_only = try(aa.value.azuread_authentication_only,null)
     }
  }
  
  version                       = var.db_version
  public_network_access_enabled = var.public_network_access_enabled
}

## Store password in keyvault, if requested
resource "azurerm_key_vault_secret" "store-dbserver-password" {
  count         = (var.key_vault_id != "") ? 1 : 0
  name          = "${var.db_servicename}-dbserver-accesskey"
  value         = random_password.db_admin_password.result
  key_vault_id  = var.key_vault_id
  #depends_on   = [azurerm_key_vault_access_policy.deployAccountAccess1]
}

## Configure databases on this service
resource "azurerm_mssql_database" "databases" {
  for_each            = local.dbs
  name                = each.key
  server_id           = azurerm_mssql_server.mServer1.id
  collation           = lookup(each.value, "collation", null)
  maintenance_configuration_name = lookup(each.value, "maintenance_configuration_name", null)

  sku_name            = lookup(each.value, "sku_name", null)
  max_size_gb         = lookup(each.value, "max_size_gb", null)
  zone_redundant      = lookup(each.value, "zone_redundant", null)
}

#
## Optional private network endpoints
#
resource "azurerm_private_endpoint" "storage_private_endpoints" {
  count                         = var.private_endpoint == {} ? 0 : 1
  name                          = "${var.db_servicename}-mssql"
  location                      = var.location
  resource_group_name           = var.resource_group_name
  subnet_id                     = data.azurerm_subnet.sn[0].id
  custom_network_interface_name = "${var.db_servicename}-mssql-nic"

  ## make the service register its DNS records in our private zone table
  private_dns_zone_group {
    name                 = "plookups"
    private_dns_zone_ids = [ var.pdnszone_ids[var.private_endpoint.pdnskey] ]
  }

  ## link this private endpoint to the service that should use it
  private_service_connection {
    name                           = "${var.db_servicename}-mssql-cn"
    private_connection_resource_id = azurerm_mssql_server.mServer1.id
    is_manual_connection           = false
    subresource_names              = var.private_endpoint.subresource_names
  }
}

## Optional subnet for private links
data "azurerm_subnet" "sn" {
  count                = var.private_endpoint == {} ? 0 : 1
  resource_group_name  = var.private_endpoint.network_resource_group_name
  virtual_network_name = var.private_endpoint.virtual_network_name
  name                 = var.private_endpoint.subnet_name
}
#
## Output section: export sql-server resources whole-sale.
#
output "mServer1" { value = azurerm_mssql_server.mServer1 }
output "databases" { value = azurerm_mssql_database.databases }