### Module to deploy MSSQL database services

This module provides options to deploy the database server and one or more databases
on it. Additionally, this modules provides:
  - generated administrator credentials that may be stored in a key vault
  - private network endpoint configuration


The following example code shows a typical way of using this module may be used
in a terraform root module.

```
#
## Manage databases and server settings defined in input file (yaml format)
#
locals {
  db_data = {
    version       = "12.0"
    admin_account = "admin"
    databases     = {
      db1 = {
        collation                      = "SQL_Latin1_General_CP1_CI_AS"
        maintenance_configuration_name = "SQL_Default"
        sku_name                       = "S0"
        max_size_gb                    = 4
        zone_redundant                 = false
      }
      db2 = {
        collation                      = "SQL_Latin1_General_CP1_CI_AS"
        maintenance_configuration_name = "SQL_Default"
        sku_name                       = "P2"
        max_size_gb                    = 7
        zone_redundant                 = true
      }
    }
  }

  pdnszone_ids = {
    for zname in local.gconf.az_platform_aac_keys:
      (zname) => local.paacconf[zname] if startswith(zname,"platform.core.pdnszone")
  }

  private_endpoint = {
    network_resource_group_name = "rg-${local.unitname}-infra"
    virtual_network_name        = "vnet-${local.unitname}"
    subnet_name                 = "snet-databases"  ## must be provisioned, eg. in lz config
    subresource_names           = [ "sqlServer" ]
    pdnskey = "platform.core.pdnszone.privatelink.database.windows.net"
  }

}

#
## Call module db/mssql-service
## Note: arrange to store db admin password in our standard keyvault
module "mssql" {
  source              = "../../lib/tf/modules/db/mssql-service-v1"
  db_servicename      = "dbs-${local.unitname}"
  db_data             = local.db_data
  location            = local.rgapp.location
  resource_group_name = local.rgapp.name
  key_vault_id        = local.aac.keyvault1_id

  private_endpoint    = local.private_endpoint
  pdnszone_ids        = local.pdnszone_ids
}
```
