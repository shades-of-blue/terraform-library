variable "db_servicename"      {}
variable "location"            {}
variable "resource_group_name" {}
variable "db_data"             {}
variable "key_vault_id"        { default = null }
variable "key_vault_key_id"    { default = null }
variable "password_length"     { default = 37 }
variable "db_version" { default = "12.0" }

variable "public_network_access_enabled" { default = true }
variable "admin_account"                 { default = "qabbala" }

variable "private_endpoint"              { default = {} }
variable "pdnszone_ids"                  { default = {} }

