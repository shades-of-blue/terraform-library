## Postgres database name + database parameters
variable "name"                  { }
variable "args"                  { }

## Azure context parameters
variable "location"              { }
variable "resource_group_name"   { }
variable "key_vault_id"          { default = null }
variable "key_vault_key_id"      { default = null }
variable "admin_password_length" { default = 37 }

variable "private_endpoint_data" { default = {} }
