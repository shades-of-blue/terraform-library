#
## Manage databases and server settings defined in input file (yaml format)
#

## Generate a db server admin password (that optionally also goes in a key vault)
resource "random_password" "db_admin_password" {
  length           = var.admin_password_length
  special          = true
  override_special = ":"
}

#
## Configure postgres service resource
## cf. https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/postgresql_flexible_server
#
resource "azurerm_postgresql_flexible_server" "server1" {
  name                      = try(var.args.dbname,var.name)
  location                  = var.location
  resource_group_name       = var.resource_group_name

  version                   = var.args.version
  sku_name                  = var.args.sku_name
  administrator_login       = var.args.admin_account
  administrator_password    = random_password.db_admin_password.result

#  identity {
#    type = "SystemAssigned"
#  }

  delegated_subnet_id       = data.azurerm_subnet.sn.id
  private_dns_zone_id       = var.private_endpoint_data.private_dnszone_id

  storage_mb                = var.args.storage_mb
  zone                      = try(var.args.zone,null)

  ## high_availability: optional single block
  dynamic "high_availability" {
    for_each = try([var.args.high_availability],{})
     iterator = ha
     content {
       mode                      = try(ha.value.mode,null)
       standby_availability_zone = try(ha.value.standby_availability_zone,null)
     }
  }

  ## authentication: optional single block
  dynamic "authentication" {
    for_each = try([var.args.authentication],{})
     iterator = aa
     content {
       active_directory_auth_enabled = try(aa.value.active_directory_auth_enabled,null)
       password_auth_enabled = try(aa.value.password_auth_enabled,null)
       tenant_id = try(aa.value.tenant_id,null)
     }
  }
  
  ## maintenance_window: optional single block
  dynamic "maintenance_window" {
    for_each = try([var.args.maintenance_window],{})
     iterator = mw
     content {
       day_of_week  = try(mw.value.day_of_week,null)
       start_hour   = try(mw.value.start_hour,null)
       start_minute = try(mw.value.start_minute,null)
     }
  }

  ## Ignore failover operations by azure
  lifecycle {
   ignore_changes = [
     zone,
     high_availability.0.standby_availability_zone
   ]
  }


}

## Store password in keyvault, if requested
resource "azurerm_key_vault_secret" "store-dbserver-password" {
  count         = (var.key_vault_id != "") ? 1 : 0
  name          = "${var.name}-dbserver-accesskey"
  value         = random_password.db_admin_password.result
  key_vault_id  = var.key_vault_id
  #depends_on   = [azurerm_key_vault_access_policy.deployAccountAccess1]
}

## Configuration items
resource "azurerm_postgresql_flexible_server_configuration" "sconfigs" {
  for_each  = try(var.args.server_parameters,{})
  server_id = azurerm_postgresql_flexible_server.server1.id
  name      = each.key
  value     = each.value
}

## Configure databases on this service
resource "azurerm_postgresql_flexible_server_database" "databases" {
  for_each  = try(var.args.databases,{})
  server_id = azurerm_postgresql_flexible_server.server1.id
  name      = each.key
  charset   = lookup(each.value, "charset", "utf8")
  collation = lookup(each.value, "collation", "en_US.utf8")
}

## Optional subnet for private links
data "azurerm_subnet" "sn" {
  resource_group_name  = var.private_endpoint_data.network_resource_group_name
  virtual_network_name = var.private_endpoint_data.virtual_network_name
  name                 = var.private_endpoint_data.subnet_name
}

#
## Output section: export database resources whole-sale.
#
output "dbserver" { value = azurerm_postgresql_flexible_server.server1 }
output "databases" { value = azurerm_postgresql_flexible_server_database.databases }

output "db_admin_password" { value = random_password.db_admin_password.result }
