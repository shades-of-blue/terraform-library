### Module to deploy postgres database services

This module provides options to deploy the database server and one or more databases
on it. Additionally, this modules provides:
  - generated administrator credentials that may be stored in a key vault
  - private network endpoint configuration


Prerequisites: a subnet delegated to the Postgres PaaS service; a private DNS zone link to the privatelink.postgres.database.azure.com namespace. Additionally, the resource Id of this private DNS zone must be passed to this module.

The following example code shows a typical way of using this module may be used
in a terraform root module.

```
#
## Manage databases and server settings defined in input file (yaml format)
#
pgdb-saphire: 

  version: '14'
  sku_name: 'B_Standard_B2s'
  storage_mb: 32768
  admin_account: 'dbadmin'

  server_parameters: { }

  maintenance_window:
    day_of_week: 1
    start_hour: 22
    start_minute: 37


  databases:
    balin:
      charset: 'utf8'

    bofur:
      collation: 'en_US.utf8'

    oakenshield: {}


#
## Terraform code calling the postgres module
#
locals {
  ## Assume the database data is in yaml file 'pgdata.yaml'
  ## with fallback location relative to TF root
  fdbdata = coalesce(
    try(file("${path.cwd}/pgdata.yaml"),null),
    try(file("${path.root}/../pgdata.yaml"),null),
    null
  )

  ## Fetch postgres database parameters
  db_name = "pgdb-saphire"
  db_data = yamldecode(local.fdbdata)
 
  ## Get private DNS zone information from platform data (cf. prologue.tf)
  pdnszone_ids = {
    for zname in local.gconf.az_platform_aac_keys:
      (zname) => local.paacconf[zname] if startswith(zname,"platform.core.pdnszone")
  }
    
  ## Note: azure/postgres uses a dedicated subnet
  private_endpoint_data = {
    network_resource_group_name = "rg-${local.unitname}-infra"
    virtual_network_name        = "vnet-${local.unitname}"
    subnet_name                 = "postgres-services"
    pdns_id                     = local.pdnszone_ids["platform.core.pdnszone.privatelink.postgres.database.azure.com"]
  }

}

#
## Call module db/postgres-service
## Note: arrange to store db admin password in our standard keyvault
#
module "pgdb" {
  source                = "../../lib/tf/modules/db/postgres-service-v1"
  name                  = local.db_name
  args                  = local.db_data[local.db_name]

  location              = local.rgapp.location
  resource_group_name   = local.rgapp.name
  key_vault_id          = local.aac.keyvault1_id
  private_endpoint_data = local.private_endpoint_data
}

```
